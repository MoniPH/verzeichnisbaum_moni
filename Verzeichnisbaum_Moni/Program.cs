﻿using System;
using System.Collections.Generic;
using System.IO;



namespace Verzeichnisbaum_Moni
{
    class Program
    {
        static void Main(string[] args)
        {
            // Hier beginnt die Suche
            string startDir = "";
            if (args.Length > 0)
            {
                startDir = args[0];
            } else
            {
                Console.WriteLine("Kein Startverzeichnis angegeben!");
                return;
            }
            // Neue Instanz von Program
            Program program = new Program();
            // Aufruf Methode Verzeichnis holen
            List<DirectoryInfo> allResulst = program.GetListing(startDir);

            foreach (DirectoryInfo info in allResulst)
            {
                // Berechnen die Größe des Verzeichnisse (Summe der enthaltenen Dateien)
                long size = program.GetDirectorySize(info);

                // Ausgabe der Verzeichnisse
                Console.WriteLine(info.ToString());
                // Ausgabe der Größe
                Console.WriteLine(size.ToString());
            }
        }

        public long GetDirectorySize(DirectoryInfo info)
        {
            long result = 0;
            // Auslesen der Dateien im Verzeichnis, dass durch info beschrieben wird
            FileInfo[] files = info.GetFiles();

            // Iteriert über alle Dateien ...
            foreach (FileInfo file in files)
            {
                // ... und addiert die Dateigröße zum Ergebnis
                result += file.Length;
            }
            // Ergebnis zurückgeben
            return result;
        }

        public List<DirectoryInfo> GetListing(string startDir)
        {
            // Rückgabewert Liste von strings
            List<DirectoryInfo> result = new List<DirectoryInfo>();

            // Verzeichnisinformationen des Anfangsverzeichnisses
            DirectoryInfo startDirInfo = new DirectoryInfo(startDir);

            // Startverzeichnis der Ergebnisliste hinzufügen
            result.Add(startDirInfo);
            // Ruft eine Liste mit DirectoryInfos zu den Unterverzeichnisese ab
            DirectoryInfo[] subDirs = startDirInfo.GetDirectories();

            // Iteriert über alle Unterverzeichnisse...
            foreach (DirectoryInfo info in subDirs)
            {
                // ... mit der Methode GetListing()
                result.AddRange(GetListing(info.FullName));
            }
            // Rückgabe der Ergebnisse
            return result;
        }
    }
}
